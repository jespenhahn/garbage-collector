/*
 * util.h
 *
 *  Created on: Jul 16, 2014
 *      Author: jesp
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdint.h>    /* for uint8_t */
#include <sys/types.h> /* for size_t */
#include <stdbool.h>   /* for bool */
#include <stdlib.h>    /* for NULL */
#include <limits.h>    /* for UINT_MAX */

#include <stdio.h>
#include <assert.h>
#include <string.h>

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;

/**
 * Round a positive number to the next highest integer
 */
#define UCEIL(x) ((int) ((x) + 0.5))

#endif /* UTIL_H_ */
