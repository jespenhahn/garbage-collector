/*
 * main.c
 *
 *  Created on: Jul 15, 2014
 *      Author: jesp
 */
#include "main.h"

int main() {
	VM* vm = newVM();

	// Create types
	Type* t_int = newType("int", 4, true);
	Type* t_pointer = newType("pointer", sizeof(uint8*));

	// Create generic fields
	Field* field_age = newField("age", t_int);
	
	// Create person class
	Field* person_fields[1] = { field_age };
	Class* person_class = newClass("person", 1, person_fields);

	Object* john = newObject(vm, person_class);
	Object* leah = newObject(vm, person_class);
	
	unsigned int temp = 18;
	set32(john, field_age, &temp);

	temp = 16;
	set32(leah, field_age, &temp);

	printf("%s = %d\n", field_age->name, *get32(john, field_age));
	printf("%s = %d\n", field_age->name, *get32(leah, field_age));

	// Free
	free(vm);

	// Exit
	puts("Done");
	exit(0);
}
