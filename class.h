/*
 * class.h
 *
 *  Created on: Jul 17, 2014
 *      Author: jesp
 */

#ifndef CLASS_H_
#define CLASS_H_

#include "util.h"

// A static type definition for a field
typedef struct Type {
	uint8 primative : 1, : 7;
	size_t size;
	char* name;
} Type;

// A static field definition for a class
typedef struct Field {
	char* name;
	Type* type;
} Field;

// A static class definition
typedef struct Class {
	char* name;
	size_t instance_size;

	size_t fields_length;
	Field** fields;
} Class;

Type* newType  (char name[], size_t size, bool primative);
Field* newField(char name[], Type* type);
Class* newClass(char name[], size_t num_fields, Field* fields[]);

size_t getFieldSize(Field* field);

#endif /* CLASS_H_ */
