/*
 * class.c
 *
 *  Created on: Jul 17, 2014
 *      Author: jesp
 */

#include "class.h"

Type* newType(char name[], size_t size, bool primative) {
	Type* type = (Type*) malloc(sizeof(Type));
	assert(type != NULL);

	type->name = name;
	type->size = size;
	type->primative = primative;

	return type;
}

Field* newField(char name[], Type* type) {
	Field* field = (Field*) malloc(sizeof(Field));
	assert(field != NULL);

	field->name = name;
	field->type = type;

	return field;
}

Class* newClass(char name[], size_t fields_length, Field* fields[]) {
	Class* class = (Class*) malloc(sizeof(Class));
	assert(class != NULL);

	class->name = name;
	class->fields_length = fields_length;
	class->fields = fields;

	size_t instance_size = 0;
	for (int i = 0; i < fields_length; i++) {
		instance_size += fields[i]->type->size;
	}
	class->instance_size = instance_size;

	return class;
}

size_t getFieldSize(Field* field) {
	return field->type->size;
}
