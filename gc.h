/*
 * gc.h
 *
 *  Created on: Jul 17, 2014
 *      Author: jesp
 */

#ifndef GC_H_
#define GC_H_

#include "util.h"
#include "class.h"

#define VM_STACK_SIZE (1024)
#define VM_G1_SIZE    (1024)

#define VM_G2_SIZE    (1024)

#define VM_G3_SIZE    (1024)
#define BIN_STEP (8)
#define BIN_LENGTH  (128)
#define BIN_LASTIDX (BIN_LENGTH - 1)

typedef struct VM VM;

typedef struct {
	uint32 marked : 1, : 30, inuse : 1;
	Class* class;

	uint8* field_vals;
} Object;

////////////////////////////////////////////////////////
// Constructors                                        /
////////////////////////////////////////////////////////

VM* newVM();
Object* newObject(VM* vm, Class* c);

void sweep(VM* vm, size_t num_gray, Object* gray[]);

uint32* get32(Object* obj, Field* field);
void set32(Object* obj, Field* field, uint32* val);

#endif /* GC_H_ */
