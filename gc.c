/*
 * gc.c
 *
 *  Created on: Jul 17, 2014
 *      Author: John Espenhahn
 */

#include "gc.h"

/* Private */ typedef struct Chunk Chunk;

// Virtual machine that contains the memory heap and stack
struct VM {
	size_t stack_idx;
	uint32 stack[VM_STACK_SIZE];

	size_t g1_idx;
	uint32 g1[VM_G1_SIZE]; // unused

	size_t g2_idx;
	uint32 g2[VM_G2_SIZE]; // unused

	size_t g3_idx;
	Chunk* g3_binheads[BIN_LENGTH];
	uint32 g3[VM_G3_SIZE];
};

// A free chunk
struct Chunk {
	uint32 size : 31, inuse : 1;
	Chunk* next;

	uint8* field_vals;
};

// An object instance / in use chunk
#define OBJECT_SIZE (sizeof(Object) - sizeof(uint8*))

/**
 * Create a new virtual machine
 * @return a new VM
 */
VM* newVM() {
	VM* vm = (VM*) malloc(sizeof(VM));
	assert(vm != NULL);

	vm->stack_idx = 0;
	vm->g1_idx = 0;
	vm->g2_idx = 0;
	vm->g3_idx = 0;

	return vm;
}

/**
 * Get the index for a chunk of chunk_size
 * @param chunk_size The size of the chunk we are looking for
 * @return The index of the bin containing chunks of size chunk_size
 */
static size_t getBinIdx(float chunk_size) {
	size_t idx = UCEIL(chunk_size / BIN_STEP);
	if (idx <= BIN_LASTIDX) {
		return idx;
	} else {
		return BIN_LASTIDX;
	}
}

/**
 * Get a free chunk of the VM from the bin at bin_idx of size obj_size
 * @param vm The virtual machine the chunk is in
 * @param bin_idx The bin to get the chunk from, the bin should not be empty
 * @param obj_size The size of the chunk/object to get
 * @return The chunk of size obj_size
 */
static Chunk* getChunk(VM* vm, size_t bin_idx, size_t obj_size) {
	Chunk* chunk = vm->g3_binheads[bin_idx];
	vm->g3_binheads[bin_idx] = chunk->next;

	if (chunk->size - obj_size >= sizeof(Chunk)) {
		Chunk* split_chunk = (Chunk*) (chunk + obj_size);
		split_chunk->size  = chunk->size - obj_size;
		split_chunk->inuse = 0;

		size_t split_bin_idx = getBinIdx((float) split_chunk->size);
		Chunk* split_lasthead = vm->g3_binheads[split_bin_idx];
		vm->g3_binheads[split_bin_idx] = split_chunk;

		split_chunk->next = split_lasthead;
	}

	return chunk;
}

/**
 * Create an object of type Class c in the VM vm
 * @param vm The virtual machine* the object is in
 * @param c The class* of the object
 * @return The object*
 */
Object* newObject(VM* vm, Class* c) {
	int obj_bytes = OBJECT_SIZE + c->instance_size;
	int obj_size = UCEIL((float) obj_bytes / sizeof(uint32));

	Object* obj;
	if (vm->g3_idx + obj_size < VM_G3_SIZE) {
		// printf("Create object of type %s (size = %d) on stack\n", c->name, obj_size);
	
		obj = (Object*) (vm->g3 + vm->g3_idx);

		vm->g3_idx += obj_size;
	} else if (obj_size < BIN_STEP*64) {
		// puts("Create object from bin");
	
		int bin_idx = getBinIdx((float) obj_size);
		while (vm->g3_binheads[bin_idx] == NULL && bin_idx < BIN_LASTIDX) {
			bin_idx += 1;
		}
		assert(vm->g3_binheads[bin_idx] != NULL);

		Chunk* chunk = getChunk(vm, bin_idx, obj_size);
		obj = (Object*) chunk;
	} else {
		printf("Could not create instance of %s.\n %s at line %d", c->name, __FILE__, __LINE__);
		exit(1);
	}

	obj->marked = 0;
	obj->inuse  = 1;
	obj->class  = c;

	return obj;
}

/**
 * The main GC function
 * @param vm The VM
 * @param num_gray The size of the array 'gray'
 * @param gray An array of gray objects of size 'num_gray'
 */
void sweep(VM* vm, size_t num_gray, Object* gray[]) {
	for (int i = 0; i < num_gray; i++) {
		Object* obj = gray[i];
		mark(vm, obj);
	}

	// TODO free unmarked
}

/**
 * Turn a gray object black by marking all of the objects it points to
 * @param vm The VM
 * @param obj THe gray object to turn black
 */
void mark(VM* vm, Object* obj) {
	if (obj->marked == 1) return;

	obj->marked = 1;

	Class* class = obj->class;

	int field_vals_idx = 0;
	size_t num_fields = class->fields_length;
	for (int j = 0; j < num_fields; j++) {
		if (class->fields[j]->type->primative == false) {
			Object* subobj = (Object*) (&obj->field_vals + field_vals_idx);
			mark(vm, subobj);
		}

		field_vals_idx += getFieldSize(class->fields[j]);
	}
}

uint32* get32(Object* obj, Field* field) {
	int field_idx = 0;
	int field_vals_idx = 0;
	
	Class* class = obj->class;
	int num_fields = class->fields_length;
	
	for (; field_idx < num_fields; field_idx++) {
		if (strcmp(field->name, class->fields[field_idx]->name) == 0) {
			break;
		} else {
			field_vals_idx += getFieldSize(class->fields[field_idx]);
		}
	}
	
	return (uint32*) (&obj->field_vals + field_vals_idx);
}

void set32(Object* obj, Field* field, uint32* val) {
	uint32* targ = get32(obj, field);
	
	int size = field->type->size / sizeof(uint32);
	for (int i = 0; i < size; i++) {
		*(targ + i) = *(val + i);
	}
}
